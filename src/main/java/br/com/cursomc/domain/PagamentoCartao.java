package br.com.cursomc.domain;

import javax.persistence.Entity;

import br.com.cursomc.domain.enums.EstadoPagamento;
@Entity
public class PagamentoCartao extends Pagamento{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer numeroParcelas;
	
	public PagamentoCartao() {
		
	}

	public PagamentoCartao(Integer id, EstadoPagamento estadoPgt, Pedido pedido, Integer numeroParcelas) {
		super(id, estadoPgt, pedido);
		this.numeroParcelas = numeroParcelas;
		// TODO Auto-generated constructor stub
	}

	public Integer getNumeroParcelas() {
		return numeroParcelas;
	}

	public void setNumeroParcelas(Integer numeroParcelas) {
		this.numeroParcelas = numeroParcelas;
	}
}
